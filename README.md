# Minecraft Docker image builder

Minecraft Docker image builder (MCDIB) is a project that creates Docker images to build Minecraft servers running Spigot, Paper, etc... in 1.8.8 and Minecraft proxies running BungeeCord, Velocity or Waterfall. Docker images built in this way are made to be configured and support some patches.

Actually, for the Minecraft minecraft servers the only disponible version is the 1.8.8 but in the future i'll improve this and move the syntax of the tags of the image to **registry.gitlab.com/docker-publics/mcdib/paper:[version]**, **registry.gitlab.com/docker-publics/mcdib/paper:1.8.8** for example. Also, only the version based on Bukkit are available. The addition of the versions which support the mods is not planed.

## Apply a patch

To apply a patch to one MCDIB image, there is two main ways. You can either clone the project and directly modify the content of the "build" folder with the wished patch or create a Dockerfile that imports the wished image (disponible at registry.gitlab.com/docker-publics/mcdib/[type]:[version]) and then copies the content of a folder, named "patch" for example, containing the files you wish to modify inside the /config folder or inside the /server folder, depending on wether you're using a Minecraft server image or a Minecraft proxy image. You can also modify any environment variable if the default ones don't suit you.

## Environment variables

Here's a list of all variables that have been modified according to the default image : *itzg/minecraft-server* or *itzg/bungeecord*.

For the two images mentioned above, the modified vars are :
    - TZ to Europe/Paris ;
    - DISABLE_HEALTHCHECK to TRUE ;
    - JVM_XX_OPTS TO "-Dfile.encoding=UTF-8" ;
    - ENABLE_RCON to FALSE.

For the *itzg/minecraft-server* image only, the modified vars are :
    - EULA to TRUE.

Notice that if you use the "custom" version of this image you **have to** specify the CUSTOM_SERVER var with either a file path or an url.

# Available tags

**registry.gitlab.com/docker-publics/mcdib/paper:1.8.8** and **registry.gitlab.com/docker-publics/mcdib/velocity:3.2.0**.

The addition of the BungeeCord and the Waterfall version with the tags **registry.gitlab.com/docker-publics/mcdib/bungeecord:[version]** and **registry.gitlab.com/docker-publics/mcdib/waterfall:[version]** is planed.

# Credits

**This project is highly dependent on the Docker images *itzg/minecraft-server* and *itzg/bungeecord*, so many thanks to itzg for their work!**

**This project also use the *gcr.io/kaniko/executor* image to build the images that are sent to the GitLab registry.**
